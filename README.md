# Mukuni Coding Challenge

Welome to the Mukuni Connect Coding Challenge. This challenge will be used to assess your proficiency in front-end web develoment. You should use React to complete this challenge, which must be completed and submitted by WEdnesday 24th July, 2019. Please find the details of the challenge below.

# Details
In this challenge your goal is to create the profile page shown in the screenshot below. The page should include the funtionality to edit the mobile and office number, should have validation and display a message to inform the user that their profile information has been edited. As mentioned earlier, you may use your preffered language and tools to complete the page.

Good luck!

#Profile page UI

https://drive.google.com/open?id=11q_5HaBefAbB_MPxQ1R0pWYBkVcktVXR